<?php
/*
 * Copyright (C) 2018 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\DocStoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\DocStoreBundle\Entity\DocumentCategory;

/**
 * 
 *
 */
class LoadDocumentCategory extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 35010;
    }
    
    public function load(ObjectManager $manager)
    {
        $category = (new DocumentCategory('chill-doc-store', 10))
            ->setDocumentClass(\Chill\DocStoreBundle\Entity\PersonDocument::class)
            ->setName([
                'fr' => "Document d'identité",
                'en' => "Identity"
            ])
            ;
        
        $manager->persist($category);
        
        $category = (new DocumentCategory('chill-doc-store', 20))
            ->setDocumentClass(\Chill\DocStoreBundle\Entity\PersonDocument::class)
            ->setName([
                'fr' => "Courrier reçu",
                'en' => "Received email"
            ])
            ;
        
        $manager->persist($category);
        
        $manager->flush();
    }
}
