<?php
/*

 */
namespace Chill\DocStoreBundle\Object;

use ChampsLibres\AsyncUploaderBundle\Form\AsyncFileTransformer\AsyncFileTransformerInterface;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use Chill\DocStoreBundle\Entity\StoredObject;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ObjectToAsyncFileTransformer implements AsyncFileTransformerInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function toAsyncFile($data)
    {
        if ($data instanceof StoredObject) {
            return $data;
        }
    }

    public function toData(AsyncFileInterface $asyncFile)
    {
        $object = $this->em
            ->getRepository(StoredObject::class)
            ->findByFilename($asyncFile->getObjectName())
            ;
        
        return $object ?? (new StoredObject())
            ->setFilename($asyncFile->getObjectName())
            ;
    }
}
