<?php
/*
 */
namespace Chill\DocStoreBundle\Object;

use ChampsLibres\AsyncUploaderBundle\Persistence\PersistenceCheckerInterface;
use Chill\DocStoreBundle\Entity\StoredObject;
use Doctrine\ORM\EntityManagerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class PersistenceChecker implements PersistenceCheckerInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    public function isPersisted($object_name): bool
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('COUNT(m)')
            ->from(StoredObject::class, 'm')
            ->where($qb->expr()->eq('m.filename', ':object_name'))
            ->setParameter('object_name', $object_name)
            ;
        
        return 1 === $qb->getQuery()->getSingleScalarResult();
    }
}
