<?php

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\DocStoreBundle\Form\DocumentCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/admin/document/category")
 */
class DocumentCategoryController extends Controller
{
    /**
     * @Route("/", name="document_category_index", methods="GET")
     */
    public function index(): Response
    {
        $em  = $this->getDoctrine()->getManager();
        $categories = $em->getRepository("ChillDocStoreBundle:DocumentCategory")->findAll();

        return $this->render(
            'ChillDocStoreBundle:DocumentCategory:index.html.twig',
            ['document_categories' => $categories]);
    }

    /**
     * @Route("/new", name="document_category_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $documentCategory = new DocumentCategory();
        $documentCategory
            ->setBundleId('Chill\DocStoreBundle\ChillDocStoreBundle');
        $documentCategory
            ->setIdInsideBundle(
                $em->getRepository("ChillDocStoreBundle:DocumentCategory")
                    ->nextIdInsideBundle());
        $documentCategory
            ->setDocumentClass(PersonDocument::class);

        $form = $this->createForm(DocumentCategoryType::class, $documentCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($documentCategory);
            $em->flush();

            return $this->redirectToRoute('document_category_index');
        } else {
            $documentCategory->setBundleId(
                'Chill\DocStoreBundle\ChillDocStoreBundle');
        }

        return $this->render('ChillDocStoreBundle:DocumentCategory:new.html.twig', [
            'document_category' => $documentCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{bundleId}/{idInsideBundle}", name="document_category_show", methods="GET")
     */
    public function show($bundleId, $idInsideBundle): Response
    {
        $em = $this->getDoctrine()->getManager();
        $documentCategory = $em
            ->getRepository("ChillDocStoreBundle:DocumentCategory")
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]);

        return $this->render(
            'ChillDocStoreBundle:DocumentCategory:show.html.twig',
            ['document_category' => $documentCategory]);
    }

    /**
     * @Route("/{bundleId}/{idInsideBundle}/edit", name="document_category_edit", methods="GET|POST")
     */
    public function edit(Request $request, $bundleId, $idInsideBundle): Response
    {
        $em = $this->getDoctrine()->getManager();
        $documentCategory = $em
            ->getRepository("ChillDocStoreBundle:DocumentCategory")
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]);

        $form = $this->createForm(DocumentCategoryType::class, $documentCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_category_edit', [
                'bundleId' => $documentCategory->getBundleId(),
                'idInsideBundle' => $documentCategory->getIdInsideBundle(),]);
        }

        return $this->render('ChillDocStoreBundle:DocumentCategory:edit.html.twig', [
            'document_category' => $documentCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{bundleId}/{idInsideBundle}", name="document_category_delete", methods="DELETE")
     */
    public function delete(Request $request, $bundleId, $idInsideBundle): Response
    {
        $em = $this->getDoctrine()->getManager();
        $documentCategory = $em
            ->getRepository("ChillDocStoreBundle:DocumentCategory")
            ->findOneBy(
                ['bundleId' => $bundleId, 'idInsideBundle' => $idInsideBundle]);

        if ($this->isCsrfTokenValid('delete'.$bundleId.$idInsideBundle, $request->request->get('_token'))) {
            $em->remove($documentCategory);
            $em->flush();
        }

        return $this->redirectToRoute('document_category_index');
    }
}
