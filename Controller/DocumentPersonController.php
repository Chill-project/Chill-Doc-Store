<?php

namespace Chill\DocStoreBundle\Controller;

use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\DocStoreBundle\Form\PersonDocumentType;
use Chill\DocStoreBundle\Repository\DocumentRepository;
use Chill\PersonBundle\Privacy\PrivacyEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Chill\PersonBundle\Entity\Person;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}/person/{person}/document")
 *
 * TODO faire un controller abstrait ?
 */
class DocumentPersonController extends Controller
{
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;
    
    /**
     * DocumentPersonController constructor.
     *
     * @param TranslatorInterface $translator
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(TranslatorInterface $translator, EventDispatcherInterface $eventDispatcher)
    {
        $this->translator = $translator;
        $this->eventDispatcher = $eventDispatcher;
    }
    
    /**
     * @Route("/", name="person_document_index", methods="GET")
     */
    public function index(Person $person): Response
    {
        $em = $this->getDoctrine()->getManager();

        if ($person === NULL) {
            throw $this->createNotFoundException('Person not found');
        }

        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);

        $reachableScopes = $this->get('chill.main.security.authorization.helper')
            ->getReachableScopes(
                $this->getUser(), new Role(PersonDocumentVoter::SEE),
                $person->getCenter());

        $documents = $em
            ->getRepository("ChillDocStoreBundle:PersonDocument")
            ->findBy(
                array('person' => $person, 'scope' => $reachableScopes),
                array('date' => 'DESC')
            );

        $event = new PrivacyEvent($person, array(
            'element_class' => PersonDocument::class,
            'action' => 'index'
        ));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        
        return $this->render(
            'ChillDocStoreBundle:PersonDocument:index.html.twig',
            [
                'documents' => $documents,
                'person' => $person
            ]);
    }

    /**
     * @Route("/new", name="person_document_new", methods="GET|POST")
     */
    public function new(Request $request, Person $person): Response
    {
        if ($person === NULL) {
            throw $this->createNotFoundException('person not found');
        }

        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);

        $document = new PersonDocument();
        $document->setUser($this->getUser());
        $document->setPerson($person);
        $document->setDate(new \DateTime('Now'));

        $form = $this->createForm(PersonDocumentType::class, $document, array(
            'center' => $document->getCenter(),
            'role'   => new Role('CHILL_PERSON_DOCUMENT_CREATE')
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->denyAccessUnlessGranted(
                'CHILL_PERSON_DOCUMENT_CREATE', $document,
                'creation of this activity not allowed');

            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();
            
            $this->addFlash('success', $this->translator->trans("The document is successfully registered"));

            return $this->redirectToRoute('person_document_index', ['person' => $person->getId()]);
        } elseif ($form->isSubmitted() and !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans("This form contains errors"));
        }

        return $this->render('ChillDocStoreBundle:PersonDocument:new.html.twig', [
            'document' => $document,
            'form' => $form->createView(),
            'person' => $person,
        ]);
    }

    /**
     * @Route("/{id}", name="person_document_show", methods="GET")
     */
    public function show(Person $person, PersonDocument $document): Response
    {
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $person);
        $this->denyAccessUnlessGranted('CHILL_PERSON_DOCUMENT_SEE', $document);
    
        $event = new PrivacyEvent($person, array(
            'element_class' => PersonDocument::class,
            'element_id' => $document->getId(),
            'action' => 'show'
        ));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        
        return $this->render(
            'ChillDocStoreBundle:PersonDocument:show.html.twig',
            ['document' => $document, 'person' => $person]);
    }

    /**
     * @Route("/{id}/edit", name="person_document_edit", methods="GET|POST")
     */
    public function edit(Request $request, Person $person, PersonDocument $document): Response
    {
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $person);
        $this->denyAccessUnlessGranted('CHILL_PERSON_DOCUMENT_UPDATE', $document);

        $document->setUser($this->getUser());
        $document->setDate(new \DateTime('Now'));

        $form = $this->createForm(
            PersonDocumentType::class, $document, array(
                'center' => $document->getCenter(),
                'role'   => new Role('CHILL_PERSON_DOCUMENT_UPDATE'),
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', $this->translator->trans("The document is successfully updated"));
    
            $event = new PrivacyEvent($person, array(
                'element_class' => PersonDocument::class,
                'element_id' => $document->getId(),
                'action' => 'update'
            ));
            $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
            
            return $this->redirectToRoute(
                'person_document_edit',
                ['id' => $document->getId(), 'person' => $person->getId()]);
        
        } elseif ($form->isSubmitted() and !$form->isValid()) {
            $this->addFlash('error', $this->translator->trans("This form contains errors"));
        }
    
        $event = new PrivacyEvent($person, array(
            'element_class' => PersonDocument::class,
            'element_id' => $document->getId(),
            'action' => 'edit'
        ));
        $this->eventDispatcher->dispatch(PrivacyEvent::PERSON_PRIVACY_EVENT, $event);
        
        return $this->render(
            'ChillDocStoreBundle:PersonDocument:edit.html.twig',
            [
                'document' => $document,
                'form' => $form->createView(),
                'person' => $person,
        ]);
    }

    /**
     * @Route("/{id}", name="person_document_delete", methods="DELETE")
     */
    public function delete(Request $request, Person $person, PersonDocument $document): Response
    {
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $person);
        $this->denyAccessUnlessGranted('CHILL_PERSON_DOCUMENT_DELETE', $document);

        if ($this->isCsrfTokenValid('delete'.$document->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush();
        }

        return $this->redirectToRoute(
            'person_document_index', ['person' => $person->getId()]);
    }
}
