<?php

/*
 * Copyright (C) 2018 Champs-Libres SCRLFS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\DocStoreBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\PersonBundle\Entity\Person;

/**
 *
 */
class PersonDocumentVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    const CREATE = 'CHILL_PERSON_DOCUMENT_CREATE';
    const SEE    = 'CHILL_PERSON_DOCUMENT_SEE';
    const SEE_DETAILS = 'CHILL_PERSON_DOCUMENT_SEE_DETAILS';
    const UPDATE = 'CHILL_PERSON_DOCUMENT_UPDATE';
    const DELETE = 'CHILL_PERSON_DOCUMENT_DELETE';

    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;

    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    public function getRoles()
    {
        return [
            self::CREATE,
            self::SEE,
            self::SEE_DETAILS,
            self::UPDATE,
            self::DELETE
        ];
    }
    
    protected function supports($attribute, $subject)
    {
        if (\in_array($attribute, $this->getRoles()) && $subject instanceof PersonDocument) {
            return true;
        }
        
        if ($subject instanceof Person && $attribute === self::CREATE) {
            return true;
        }
        
        return false;
    }

    protected function isGranted($attribute, $report, $user = null)
    {
        if (! $user instanceof \Chill\MainBundle\Entity\User){
            return false;
        }

        return $this->helper->userHasAccess($user, $report, $attribute);
    }

    public function getRolesWithoutScope()
    {
        return array();
    }


    public function getRolesWithHierarchy()
    {
        return ['PersonDocument' => $this->getRoles() ];
    }
}
