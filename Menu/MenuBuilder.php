<?php
/*
 */
namespace Chill\DocStoreBundle\Menu;

use Chill\MainBundle\Routing\LocalMenuBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Knp\Menu\MenuItem;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MenuBuilder implements LocalMenuBuilderInterface
{
    /**
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    public function __construct(
        TokenStorageInterface $tokenStorage, 
        AuthorizationHelper $authorizationHelper, 
        TranslatorInterface $translator
    ){
        $this->tokenStorage = $tokenStorage;
        $this->authorizationHelper = $authorizationHelper;
        $this->translator = $translator;
    }

    
    public function buildMenu($menuId, MenuItem $menu, array $parameters)
    {
        switch($menuId) {
            case 'person':
                $this->buildMenuPerson($menu, $parameters);
                break;
            default:
                throw new \LogicException("this menuid $menuId is not implemented");
        }
    }
    
    protected function buildMenuPerson(MenuItem $menu, array $parameters)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $parameters['person'];
        $user = $this->tokenStorage->getToken()->getUser();
        
        if ($this->authorizationHelper->userHasAccess($user, 
            $person->getCenter(), PersonDocumentVoter::SEE)) {
            
            $menu->addChild($this->translator->trans('Documents'),  [
                    'route' => 'person_document_index',
                    'routeParameters' => [
                        'person' => $person->getId()
                    ]
                ])
                ->setExtras([
                    'order'=> 350
                ]);
        }
        
    }

    public static function getMenuIds(): array
    {
        return [ 'person' ];
    }
}
