<?php
/*
 * 
 */
namespace Chill\DocStoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use ChampsLibres\AsyncUploaderBundle\Validator\Constraints\AsyncFileExists;

/**
 * Represent a document stored in an object store 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * 
 * @ORM\Entity()
 * @ORM\Table("chill_doc.stored_object")
 * @AsyncFileExists(
 *  message="The file is not stored properly"
 * )
 */
class StoredObject implements AsyncFileInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text")
     */
    private $filename;
    
    /**
     * @ORM\Column(type="json_array", name="key")
     * @var array
     */
    private $keyInfos = array();
    
    /**
     *
     * @var int[] 
     * @ORM\Column(type="json_array", name="iv")
     */
    private $iv = array();
    
    /**
     *
     * @var \DateTime
     * @ORM\Column(type="datetime", name="creation_date")
     */
    private $creationDate;
    
    /**
     *
     * @var string 
     * @ORM\Column(type="text", name="type")
     */
    private $type = '';
    
    /**
     *
     * @var array
     * @ORM\Column(type="json_array", name="datas")
     */
    private $datas = [];
    
    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getDatas()
    {
        return $this->datas;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
        
        return $this;
    }

    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }

    public function setDatas(array $datas)
    {
        $this->datas = $datas;
        
        return $this;
    }

    public function getObjectName()
    {
        return $this->getFilename();
    }
    
    public function getKeyInfos()
    {
        return $this->keyInfos;
    }

    public function getIv()
    {
        return $this->iv;
    }

    public function setKeyInfos($keyInfos)
    {
        $this->keyInfos = $keyInfos;
        
        return $this;
    }

    public function setIv($iv)
    {
        $this->iv = $iv;
        
        return $this;
    }


}
