<?php

namespace Chill\DocStoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\PersonBundle\Entity\Person;


/**
 * @ORM\Table("chill_doc.person_document")
 * @ORM\Entity()
 */
class PersonDocument extends Document implements HasCenterInterface, HasScopeInterface
{
    /**
     * @ORM\ManyToOne(targetEntity="Chill\PersonBundle\Entity\Person")
     * @var Person
     */
    private $person;

    /**
     * Get person
     *
     * @return \Chill\MainBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    public function setPerson($person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getCenter()
    {
        return $this->getPerson()->getCenter();
    }
}
