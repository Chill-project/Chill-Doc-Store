<?php

namespace Chill\DocStoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("chill_doc.document_category")
 * @ORM\Entity(repositoryClass="Chill\DocStoreBundle\EntityRepository\DocumentCategoryRepository")
 */
class DocumentCategory
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", name="bundle_id")
     * @var string The id of the bundle that has create the category (i.e. 'person', 'activity', ....)
     */
    private $bundleId;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", name="id_inside_bundle")
     * @var int The id which is unique inside the bundle
     */
    private $idInsideBundle;

    /**
     * @ORM\Column(type="string", name="document_class")
     * @var string The class of the document (ie Chill\DocStoreBundle\PersonDocument)
     */
    private $documentClass;


    /**
     * @ORM\Column(type="json_array")
     */
    private $name;
    
    public function __construct($bundleId, $idInsideBundle)
    {
        $this->bundleId = $bundleId;
        $this->idInsideBundle = $idInsideBundle;
    }

        public function getBundleId() // ::class BundleClass (FQDN)
    {
        return $this->bundleId;
    }

    public function getIdInsideBundle()
    {
        return $this->idInsideBundle;
    }

    public function getDocumentClass()
    {
        return $this->documentClass;
    }

    public function setDocumentClass($documentClass): self
    {
        $this->documentClass = $documentClass;

        return $this;
    }

    public function getName($locale = null)
    {
        if ($locale) {
            if (isset($this->name[$locale])) {
                return $this->name[$locale];
            } else {
                foreach ($this->name as $name) {
                    if (!empty($name)) {
                        return $name;
                    }
                }
            }
            return '';
        } else {
            return $this->name;
        }
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }
}
