<?php

namespace Chill\DocStoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\User;
use ChampsLibres\AsyncUploaderBundle\Validator\Constraints\AsyncFileExists;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
class Document implements HasScopeInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *  min=2, max=250
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description = '';

    /**
     * @ORM\ManyToOne(targetEntity="Chill\DocStoreBundle\Entity\DocumentCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_bundle_id", referencedColumnName="bundle_id"),
     *   @ORM\JoinColumn(name="category_id_inside_bundle", referencedColumnName="id_inside_bundle")
     * })
     */
    private $category;

    /**
     * @ORM\ManyToOne(
     *  targetEntity="Chill\DocStoreBundle\Entity\StoredObject",
     *  cascade={"persist"}
     * )
     * @Assert\Valid()
     * @Assert\NotNull(
     *  message="Upload a document"
     * )
     */
    private $object;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\Scope")
     * @var \Chill\MainBundle\Entity\Scope The document's center
     */
    private $scope;

    /**
     * @ORM\ManyToOne(targetEntity="Chill\MainBundle\Entity\User")
     * @var \Chill\PersonBundle\Entity\user The user who encoded the exif_read_data
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId()
    {
        return $this->id;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = (string) $description;

        return $this;
    }

    /**
     * @return DocumentCategory
     */
    public function getCategory(): ?DocumentCategory
    {
        return $this->category;
    }

    public function setCategory(DocumentCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get scope
     *
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    public function setScope($scope): self
    {
        $this->scope = $scope;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
    
    public function getObject(): ?StoredObject
    {
        return $this->object;
    }
    
    public function setObject(StoredObject $object = null)
    {
        $this->object = $object;
        
        return $this;
    }
}
