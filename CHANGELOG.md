
Version 1.5.1
=============

- adding .gitlab-ci to upgrade automatically packagist
- adding fixtures for ACL and DocumentCategory

Version 1.5.2
=============

- fix some missing translations on update / create document and "any document" in list
- use dropzone to upload a document with a better UI

You must add `"dropzone": "^5.5.1"` to your dependencies in `packages.json` at the root project.

Version 1.5.3
=============

- the javascript for uploading a file now works within collections, listening to collection events.

Version 1.5.4
=============

- replace default message on download button below dropzone ;
- launch event when dropzone is initialized, to allow to customize events on dropzone;
- add privacy events to document index / show
- add privacy events to document edit / update
- remove dump message

Version 1.5.5
=============

- add button to remove existing document in form, and improve UI in this part
- fix error when document is removed in form

Master branch
=============

- fix capitalization of person document pages
