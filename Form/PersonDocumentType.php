<?php

namespace Chill\DocStoreBundle\Form;


use Chill\DocStoreBundle\Entity\Document;
use Chill\DocStoreBundle\Entity\DocumentCategory;
use Chill\DocStoreBundle\Entity\PersonDocument;
use Chill\MainBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Form\Type\AppendScopeChoiceTypeTrait;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Form\Type\ChillDateType;
use Chill\MainBundle\Form\Type\ScopePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class PersonDocumentType extends AbstractType
{
    /**
     * the user running this form
     *
     * @var User
     */
    protected $user;

    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;

    /**
     *
     * @var ObjectManager
     */
    protected $om;

    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;

    public function __construct(
        TranslatableStringHelper $translatableStringHelper
        )
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('object', StoredObjectType::class, [
                'error_bubbling' => true
            ])
            ->add('scope', ScopePickerType::class, [
                'center' => $options['center'],
                'role' => $options['role']
            ])
            ->add('date', ChillDateType::class)
            ->add('category', EntityType::class, array(
                'placeholder' => 'Choose a document category',
                'class' => 'ChillDocStoreBundle:DocumentCategory',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.documentClass = :docClass')
                        ->setParameter('docClass', PersonDocument::class);
                },
                'choice_label' => function ($entity = null) {
                    return $entity ? $this->translatableStringHelper->localize($entity->getName()) : '';
                },
            ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
        
        $resolver->setRequired(['role', 'center'])
            ->setAllowedTypes('role', [ \Symfony\Component\Security\Core\Role\Role::class ])
            ->setAllowedTypes('center', [ \Chill\MainBundle\Entity\Center::class ])
            ;
    }
}
