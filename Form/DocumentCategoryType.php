<?php

namespace Chill\DocStoreBundle\Form;

use Chill\DocStoreBundle\Entity\DocumentCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;

class DocumentCategoryType extends AbstractType
{
    private $chillBundlesFlipped;

    public function __construct($kernelBundles)
    {
        // TODO faire un service dans CHillMain
        foreach ($kernelBundles as $key => $value) {
            if(substr($key, 0, 5) === 'Chill') {
                $this->chillBundlesFlipped[$value] = $key;
            }
        }
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bundleId', ChoiceType::class, array(
                'choices' => $this->chillBundlesFlipped,
                'disabled' => true,
            ))
            ->add('idInsideBundle', null, array(
                'disabled' => true,
            ))
            ->add('documentClass', null, array(
                'disabled' => true,
            )) // cahcerh par default PersonDocument
            ->add('name', TranslatableStringFormType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DocumentCategory::class,
        ]);
    }
}
