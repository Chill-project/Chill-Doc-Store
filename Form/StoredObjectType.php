<?php
/*
 */
namespace Chill\DocStoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use ChampsLibres\AsyncUploaderBundle\Form\Type\AsyncUploaderType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\DocStoreBundle\Entity\StoredObject;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Form type which allow to join a document 
 *
 */
class StoredObjectType extends AbstractType
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('filename', AsyncUploaderType::class)
            ->add('type', HiddenType::class)
            ->add('keyInfos', HiddenType::class)
            ->add('iv', HiddenType::class)
            ;
        
        $builder
            ->get('keyInfos')
            ->addModelTransformer(new CallbackTransformer(
                [$this, 'transform'], [$this, 'reverseTransform']
        ));
        $builder
            ->get('iv')
            ->addModelTransformer(new CallbackTransformer(
                [$this, 'transform'], [$this, 'reverseTransform']
        ));
        
        $builder
            ->addModelTransformer(new CallbackTransformer(
                [ $this, 'transformObject'], [$this, 'reverseTransformObject']
        ));
    }
    
    public function getBlockPrefix()
    {
        return 'stored_object';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', StoredObject::class)
            ;
    }
    
    public function reverseTransform($value)
    {
        if ($value === null) {
            return null;
        }
        
        return \json_decode($value, true);
    }
    
    public function transform($object)
    {
        if ($object === null) {
            return null;
        }
        
        return \json_encode($object);
    }
    
    public function transformObject($object = null)
    {
        return $object;
    }
    
    public function reverseTransformObject($object)
    { 
        if (NULL === $object) {
            return null;
        }
        
        if (NULL === $object->getFilename()) {
            // remove the original object
            $this->em->remove($object);
            
            return null;
        }
        
        return $object;
    }
}
