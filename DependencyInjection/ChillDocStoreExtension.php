<?php

namespace Chill\DocStoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\DocStoreBundle\Security\Authorization\PersonDocumentVoter;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillDocStoreExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/media.yml');
        $loader->load('services/controller.yml');
        $loader->load('services/menu.yml');
        $loader->load('services/fixtures.yml');
        $loader->load('services/form.yml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoute($container);
        $this->prependAuthorization($container);
        $this->prependTwig($container);
    }
    
    protected function prependRoute(ContainerBuilder $container)
    {
        //declare routes for task bundle
        $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                  '@ChillDocStoreBundle/Resources/config/routing.yml',
                  '@ChampsLibresAsyncUploaderBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
               PersonDocumentVoter::UPDATE => [PersonDocumentVoter::SEE_DETAILS],
               PersonDocumentVoter::CREATE => [PersonDocumentVoter::SEE_DETAILS],
               PersonDocumentVoter::DELETE => [PersonDocumentVoter::SEE_DETAILS],
               PersonDocumentVoter::SEE_DETAILS => [PersonDocumentVoter::SEE],
           )
        ));
    }
    
    protected function prependTwig(ContainerBuilder $container)
    {
        $twigConfig = array(
            'form_themes' => array('@ChillDocStore/Form/fields.html.twig')
        );
        $container->prependExtensionConfig('twig', $twigConfig);
    }
}
